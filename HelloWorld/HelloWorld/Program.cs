﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace HelloWorld
{
    class Program
    {
        static int age = 16;
        static float decimals = 3.33f;
        static string name = "Will";
        // these are variables //
        

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("my age is " + age);
            Console.WriteLine("I have " + decimals + " fingers");
            Console.WriteLine("my name is " + name);
            List<int> WillNums = new List<int>();
            WillNums.Add(1);
            WillNums.Add(5);
            WillNums.Add(7);
            WillNums.Add(3);
            Console.WriteLine("WillNums are " + WillNums[0] +  WillNums[1] +  WillNums[2] +  WillNums[3] );
            
        }
    }
}
